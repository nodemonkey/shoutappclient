'use strict';

/**
 * @ngdoc overview
 * @name shoutclientApp
 * @description
 * # shoutclientApp
 *
 * Main module of the application.
 */
angular
  .module('shoutclientApp', [
    'ngCookies',
    'ngRoute',
    'restangular'
  ])
  .config(function ($routeProvider, RestangularProvider) {
    RestangularProvider.setBaseUrl('http://localhost:3000');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'AuthCtrl',
        controllerAs: 'auth',
        allowAnonymous: true 
      })
      .when('/shouts', {
        templateUrl: 'views/shouts.html',
        controller: 'ShoutsCtrl',
        controllerAs: 'shouts'
      })
      .when('/create/shout', {
        templateUrl: 'views/shout-add.html',
        controller: 'ShoutAddCtrl',
        controllerAs: 'shoutAdd'
      })
      .when('/shout/:id', {
        templateUrl: 'views/shout-delete.html',
        controller: 'ShoutDeleteCtrl',
        controllerAs: 'shoutDelete'
      })
      .when('/shout/:id', {
        templateUrl: 'views/shout-view.html',
        controller: 'ShoutViewCtrl',
        controllerAs: 'shoutView'
      })
      .when('/shout/:id/delete', {
        templateUrl: 'views/shout-delete.html',
        controller: 'ShoutDeleteCtrl',
        controllerAs: 'shoutDelete'
      })
      .when('/shout/:id/edit', {
        templateUrl: 'views/shout-edit.html',
        controller: 'ShoutEditCtrl',
        controllerAs: 'shoutEdit'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).factory('ShoutRestangular', function(Restangular) {
      return Restangular.withConfig(function(RestangularConfigurer) {
          id: '_id'    
      });
  }).factory('Shout', function(ShoutRestangular) {
    return ShoutRestangular.service('shout');
  }).factory('AuthService', function($http, Session){
    var  authService = {};
    authService.login = function (credentials) {
         return $http
            .post('/login', credentials)
            .then(function (res) {
                Session.create(res.data.id, res.data.user.id);
                return res.data.user;
            });
     };
     authService.isAuthenticated = function () {
         return !!Session.userId;
     };
     return authService;
      
  }).service('Session', function () {
        this.create = function (sessionId, userId) {
            this.id = sessionId;
            this.userId = userId;
        };
        this.destroy = function () {
        this.id = null;
        this.userId = null;
  };
});
