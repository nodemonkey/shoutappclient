'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:ShoutsCtrl
 * @description
 * # ShoutsCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('ShoutsCtrl', function ($scope, Shout) {
      $scope.shouts = Shout.getList().$object;
  });

