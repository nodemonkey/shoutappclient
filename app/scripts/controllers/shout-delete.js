'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:ShoutDeleteCtrl
 * @description
 * # ShoutDeleteCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('ShoutDeleteCtrl', function (
    $scope,
    $routeParams,
    Shout,
    $location
  ) {
      $scope.shout = Shout.one($routeParams.id).get().$object;
      $scope.deleteShout = function() {
          $scope.shout.remove().then(function() {
            $location.path('/shout');
          });
      };
      $scope.back = function() {
          $location.path('/shout/' + $routeParams.id);
      };
  });
