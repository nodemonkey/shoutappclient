'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
