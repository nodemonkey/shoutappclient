'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:ShoutAddCtrl
 * @description
 * # ShoutAddCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('ShoutAddCtrl', function (
	$scope,
    $routeParams
    Shout,
  ){
     $scope.shout = {};
     $scope.saveShout = function() {
        Shout.post($scope.shout).then(function() {
	  $location.path('/shouts');
        });
      };
});
