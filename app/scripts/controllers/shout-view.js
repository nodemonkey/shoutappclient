'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:ShoutViewCtrl
 * @description
 * # ShoutViewCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('ShoutViewCtrl', function (
    $scope,
    $routeParams,
    Shout
  ) {
      $scope.viewShout = true;
      $scope.shout = Shout.one($routeParams.id).get().$object;
  });
