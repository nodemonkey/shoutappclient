'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
