'use strict';

/**
 * @ngdoc function
 * @name shoutclientApp.controller:ShoutEditCtrl
 * @description
 * # ShoutEditCtrl
 * Controller of the shoutclientApp
 */
angular.module('shoutclientApp')
  .controller('ShoutEditCtrl', function (
      $scope,
      $routeParams,
      Shout,
      $location
  ) {
      $scope.editShout = true;
      $scope.shout = {};
      Shout.one($routeParams.id).get().then(function (shout) {
        $scope.shout = shout;
        $scope.saveShout = function() {
            $scope.shout.save().then(function() {
                $location.path('/shout/' + $routeParams.id);
            });
       };

  });
