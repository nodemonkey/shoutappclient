'use strict';

describe('Controller: ShoutViewCtrl', function () {

  // load the controller's module
  beforeEach(module('shoutclientApp'));

  var ShoutViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShoutViewCtrl = $controller('ShoutViewCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShoutViewCtrl.awesomeThings.length).toBe(3);
  });
});
