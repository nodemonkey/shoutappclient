'use strict';

describe('Controller: ShoutAddCtrl', function () {

  // load the controller's module
  beforeEach(module('shoutclientApp'));

  var ShoutAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShoutAddCtrl = $controller('ShoutAddCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShoutAddCtrl.awesomeThings.length).toBe(3);
  });
});
