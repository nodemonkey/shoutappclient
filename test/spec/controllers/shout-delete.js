'use strict';

describe('Controller: ShoutDeleteCtrl', function () {

  // load the controller's module
  beforeEach(module('shoutclientApp'));

  var ShoutDeleteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShoutDeleteCtrl = $controller('ShoutDeleteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShoutDeleteCtrl.awesomeThings.length).toBe(3);
  });
});
