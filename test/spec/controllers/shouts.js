'use strict';

describe('Controller: ShoutsCtrl', function () {

  // load the controller's module
  beforeEach(module('shoutclientApp'));

  var ShoutsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShoutsCtrl = $controller('ShoutsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShoutsCtrl.awesomeThings.length).toBe(3);
  });
});
