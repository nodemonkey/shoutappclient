'use strict';

describe('Controller: ShoutEditCtrl', function () {

  // load the controller's module
  beforeEach(module('shoutclientApp'));

  var ShoutEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShoutEditCtrl = $controller('ShoutEditCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShoutEditCtrl.awesomeThings.length).toBe(3);
  });
});
