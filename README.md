## Basahin nang mabuti :) 

# shoutclient

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

*Install grunt first globally `npm install grunt --global`*

*Then, run `grunt` to build everything and 
to preview, run `grunt serve`.*



## Works with shoutbucks (server side)

Please check the node server https://bitbucket.org/nodemonkey/shoutbucks

## TODOS:

I was not able to finish - I'm just lacking the following:

- Use sessionStorage to be saved in passport-local session of server
- check if authenticated in all shoutapp routes
- add session username to shoutapp-add so username is in the hidden field

## Testing

Running `grunt test` will run the unit tests with karma.
